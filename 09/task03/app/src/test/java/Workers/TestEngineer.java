package Workers;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestEngineer{
   MyWorkers engineer = new Engineer("Иван", "Старший инженер", 35000);

   @Test void engineerName(){
      assertEquals("Иван", engineer.getName());
   }
   @Test void engineerPosition(){
      assertEquals("Старший инженер", engineer.getPosition());
   }
   @Test void engineerSalary(){
      assertEquals(35000, engineer.getSalary());
   }
   @Test void engineerMyWork(){
      assertEquals("Я разрабатываю и оптимизирую существующие инженерные решения", ((Engineer)engineer).myWork());
   }

}
