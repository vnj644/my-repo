package Workers;

/*
Создать классы для описания работников: работник в общем, бухгалтер, главный бухгалтер,
инженер, рабочий. Все имеют имя, должность и зарплату. Добавить характерные для каждого класса в
отдельности методы. Переопределить все доступные методы класса Object адекватным образом.
Создать массив, содержащий объекты всех классов описания работников, и вывести его на консоль.
*/

class MyMain{
   public static void main(String[] args) {
      MyWorkers[] myWorkers = new MyWorkers[4];
      myWorkers[0] = new Accountant("Лариса", "Младший бухгалтер", 45000);
      myWorkers[1] = new ChiefAccountant("Глеб", "Главный бухгалтер", 50000);
      myWorkers[2] = new Engineer("Иван", "Старший инженер", 35000);
      myWorkers[3] = new Worker("Олег", "Разнорабочий", 25000);
      for (int i = 0; i < myWorkers.length; i++) {
          System.out.println(myWorkers[i]);
      }
   }
}
