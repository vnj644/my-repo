package PolygonsText;

import java.util.Arrays;

class Polygons implements MyInterface{
   private int perimeter;
   private int[] arcs;

   public Polygons(int[] arcs)throws MyException{
      this.arcs = arcs;
      perimeterPolygons();
      checkLengthCorrect();
   }

   public int getPerimeter(){
      return this.perimeter;
   }

   public void setPerimeter(int perimeter){
      this.perimeter = perimeter;
   }

   public int[] getArcs(){
      return this.arcs;
   }

   public void setArcs(int[] arcs){
      this.arcs = arcs;
   }

   public void perimeterPolygons(){
      for (int i = 0;i < this.arcs.length ;i++ ) {
         this.perimeter += this.arcs[i];
      }
   }

   public void checkLengthCorrect()throws MyException{
      for (int i = 0; i < this.arcs.length; i++) {
         if ((this.arcs[i] * 2) > this.perimeter) {
            throw new MyException("Удвоенная дуга многоугольника не может быть больше его периметра");
         }
         if (getPerimeter() <= 0) {
            throw new MyException("Этот не геометрическая фигура");
         }
      }
   }

   public String output(){
      return "Дуги многоугольника: " + Arrays.toString(getArcs()) + ", периметр равен: " + getPerimeter();
   }
}
