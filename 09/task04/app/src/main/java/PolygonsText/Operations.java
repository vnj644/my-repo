package PolygonsText;

class Operations{
   public static MyInterface[] polygons;

   public Operations(MyInterface[] polygons) {
       this.polygons = polygons;
   }

   public static void main(String[] args) throws MyException {
      Triangle triangle = new Triangle(new int[] {1,2,3});
      Text text = new Text("text");
      MyInterface[] input = new MyInterface[]{triangle, text};
      // for (int i = 0;i < this.polygons.length ;i++ ) {
      //    System.out.println(this.polygons[i].output());
      // }
      Operations o = new Operations(input);
      o.myPrint();
   }

   public void myPrint(){
      for (int i = 0;i < this.polygons.length ;i++ ) {
         System.out.println(this.polygons[i].output());
      }
   }

}
