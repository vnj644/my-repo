package Operations2DMatrices;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TestMatrix{
   @Test void matrixTest()throws MatrixException{
      int[][] matrix1 = new int[][]{{1, 2}, {3, 4}};
      Matrix matrix2 = new Matrix(2, 2);
      matrix2.setMatrix(matrix1);
      assertArrayEquals(matrix1, matrix2.getMatrix());
   }
}
