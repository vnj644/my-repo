import java.util.*;


public class BBQ{
  public static void main(String[] args) {
    Queue<String> bbqLine = new LinkedList<String>();

    bbqLine.add("Tom");
    bbqLine.add("Jekk");
    System.out.println(bbqLine.add("Dan"));
    bbqLine.add("Bob");

    System.out.println(bbqLine.toString() + " -- toString");
    System.out.println(bbqLine.poll() + " -- poll");
    System.out.println(bbqLine.peek() + " -- peek");

    System.out.println(bbqLine);

    System.out.println(bbqLine.size() + " -- size");
    System.out.println(bbqLine.contains("Dan") + " -- contains");

    System.out.println(bbqLine.toString() + " -- toString");
    System.out.println(bbqLine.toArray()[1] + " -- toArray");


    System.out.println(bbqLine.remove());
    System.out.println(bbqLine.offer("Dan") + " -- как contains, но ещё и добавляет");

    System.out.println(bbqLine.toString() + " -- toString");
    System.out.println(bbqLine.element());

    List<String> list = Arrays.asList("красный", "синий", "aеленый");
    System.out.println("Перед сортировкой: " + list);
    Collections.sort(list);
    System.out.println("После сортировки: " + list);
    Collections.sort(list, Collections.reverseOrder());
    System.out.println("После обратной сортировки: " + list);
  }
}
