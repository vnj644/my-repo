package QueueWithoutColl;


public class Queue<E>{
   private Node<E> head;
   private Node<E> tail;
   private int realSize;
   private int supposedSize;

   MyQueue(){
      this.head = null;
      this.tail = null;
      this.realSize = 0;
      this.supposedSize = -1;
   }

   MyQueue(int supposedSize){
      this.head = null;
      this.tail = null;
      this.realSize = 0;
      this.supposedSize = supposedSize;
   }

   public int getRealSize(){
      return this.realSize;
   }

   public int getSupposedSize(){
      return this.supposedSize;
   }

   public boolean isFull(){
      return (getRealSize() == getSupposedSize());
   }

   public boolean isEmpty(){
      return (getRealSize() == 0);
   }

   public Node<E> getHead(){
      return this.head;
   }

   public Node<E> getTail(){
      return this.tail;
   }

  // ---------------------------------------------
    @Override
    public E element(){
       if (isEmpty()) {
          throw new NoSuchElementException("В Queue нет элементов");
       }
       else{
          return head.value;
       }
    }

    @Override
    public E peek(){
       if (isEmpty()) {
          return null;
       }
       else{
          return head.value;
       }
    }

  // ---------------------------------------------

    @Override
    public boolean add(E value) {
       if (supposedSize == -1) {
          Node<E> node = new Node<>(value);
          if (head == null) {
             tail = node;
             head = tail;
          }
          else {
             tail.next = node;
             tail = tail.next;
          }
          this.realSize++;
          return true;
       }
       else if (realSize <= supposedSize) {
          Node<E> node = new Node<>(value);
          if (head == null) {
             tail = node;
             head = tail;
          }
          else {
             tail.next = node;
             tail = tail.next;
          }
          this.realSize++;
          return true;
       }
       else{
          throw new IllegalStateException("Queue переполнена");
       }
    }

    @Override
    public boolean offer(E value){
       if (supposedSize == -1) {
          Node<E> node = new Node<>(value);
          if (head == null) {
             tail = node;
             head = tail;
          }
          else {
             tail.next = node;
             tail = tail.next;
          }
          this.realSize++;
          return true;
       }
       else if (realSize <= supposedSize) {
          Node<E> node = new Node<>(value);
          if (head == null) {
             tail = node;
             head = tail;
          }
          else {
             tail.next = node;
             tail = tail.next;
          }
          this.realSize++;
          return true;
       }
       else{
          return false;
       }
    }

  // ---------------------------------------------

    @Override
    public E remove() {
       if (isEmpty()) {
          throw new NoSuchElementException("В Queue нет элементов");
       }
       E temp = head.value;
       head = head.next;
       if (head == null) {
          tail = null;
       }
       this.realSize--;
       return temp;
    }

    @Override
    public E poll(){
       if (isEmpty()) {
          return null;
       }
       E temp = head.value;
       head = head.next;
       if (head == null) {
          tail = null;
       }
       this.realSize--;
       return temp;
    }
}
