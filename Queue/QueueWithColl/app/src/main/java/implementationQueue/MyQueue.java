package implementationQueue;

import java.util.Queue;
import java.util.Iterator;
import java.util.Collection;
import java.util.NoSuchElementException;

public class MyQueue<E> implements Queue<E>{

   public Node<E> head;
   public Node<E> tail;
   public int realSize;

   MyQueue(){
      this.head = null;
      this.tail = null;
      this.realSize = 0;
   }

   public int getRealSize(){
      return this.realSize;
   }


   public boolean isEmpty(){
      return (getRealSize() == 0);
   }

   // ---------------------------------------------
   // ---------------------------------------------

   @Override
   public void clear(){
      Node<E> temp;
      while (this.head != null){
         temp = this.head.getNext();
         this.head.setValue(null);
         this.head.setNext(null);
         this.head = temp;
      }
   }

   @Override
   public boolean contains(Object o){
      for (int i = 0 ;i < realSize ;i++ ) {
         Node<E> temp = head.next;
         if (temp == o) {
            return true;
         }
      }
      return false;
   }

   @Override
   public boolean containsAll(Collection c){
      for (Object i : c) {
         if (!contains(i)) {
            return true;
         }
      }
      return false;
   }

   @Override
   public boolean addAll(Collection c){
      for (Object i : c ) {
         add((E)i);
      }
      return true;
   }

   @Override
   public boolean retainAll(Collection c){
      throw new UnsupportedOperationException();
   }

   @Override
   public boolean remove(Object o){
      Node<E> node = this.head;
      Node<E> next = new Node<E>();
      try{
         while (node != null){
            if (node.getNext().getValue() == o){
               next = node.getNext();
               node.setNext(next.getNext());
               next.setValue(null);
               next.setNext(null);
               return true;
            }
            node = node.getNext();
         }
      } catch (Exception e){
         System.out.println(e);
         return false;
      }
      return false;
   }

   @Override
   public boolean removeAll(Collection c){
      try {
         E[] arr = (E[]) c.toArray();
         for (int i = 0; i < arr.length; i++) {
           this.remove(arr[i]);
         }
         return true;
      }
      catch (Exception e) {
         return false;
      }
   }

   @Override
   public Object[] toArray(){
      int size = this.size();
      Object[] mass = new Object[size];
      Node<E> node = this.head;
      for (int i = 0; i < size; i++) {
         mass[i] =  node.getValue();
         node = node.getNext();
      }
      return mass;
   }

   @Override
   public Object[] toArray(Object[] mass){
      int size = this.size();
      mass = new Object[size];
      Node<E> node = this.head;
      for (int i = 0; i < mass.length; i++) {
         mass[i] = node.getValue();
         node = node.getNext();
      }
      return mass;
   }

   @Override
   public Iterator<E> iterator() {
      return new Iterator<E>() {
         Node<E> currentNode = head;

         @Override
         public boolean hasNext() {
            return currentNode != null;
         }

         @Override
         public E next() {
            E result = currentNode.getValue();
            currentNode = currentNode.getNext();
            return result;
         }
      };
   }

   @Override
   public int size(){
      Node node = this.head;
      int size = 0;
      while (node != null) {
         size++;
         node = node.getNext();
      }
      return size;
   }

   // ---------------------------------------------
   // ---------------------------------------------

   @Override
   public E element(){
      if (isEmpty()) {
         throw new NoSuchElementException("В Queue нет элементов");
      }
      else{
         return head.value;
      }
   }

   @Override
   public E peek(){
      if (isEmpty()) {
         return null;
      }
      else{
         return head.value;
      }
   }

   // ---------------------------------------------

   @Override
   public boolean add(E value) {
      try {
         Node<E> node = new Node<>(value);
         if (head == null) {
            tail = node;
            head = tail;
         }
         else {
            tail.next = node;
            tail = tail.next;
         }
         this.realSize++;
         return true;
      } catch(Exception e) {
         throw new IllegalStateException("Queue переполнена");
      }
   }

   @Override
   public boolean offer(E value){
      try {
         Node<E> node = new Node<>(value);
         if (head == null) {
            tail = node;
            head = tail;
         }
         else {
            tail.next = node;
            tail = tail.next;
         }
         this.realSize++;
         return true;
      } catch(Exception e) {
         return false;
      }
   }

   // ---------------------------------------------

   @Override
   public E remove() {
      if (isEmpty()) {
         throw new NoSuchElementException("В Queue нет элементов");
      }
      E temp = head.value;
      head = head.next;
      if (head == null) {
         tail = null;
      }
      this.realSize--;
      return temp;
   }

   @Override
   public E poll(){
      if (isEmpty()) {
         return null;
      }
      E temp = head.value;
      head = head.next;
      if (head == null) {
         tail = null;
      }
      this.realSize--;
      return temp;
      }
   }
