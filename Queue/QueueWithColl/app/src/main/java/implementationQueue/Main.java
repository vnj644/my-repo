package implementationQueue;

class Main{
   public static void main(String[] args) {
      MyQueue<String> queue = new MyQueue<String>();
      // System.out.println(queue.add("A"));
      // System.out.println(queue.element());
      // System.out.println(queue.remove());
      // System.out.println(queue.offer("B"));
      // System.out.println(queue.peek());
      // System.out.println(queue.poll());
      queue.add("Tom");

      queue.add("Jekk");

      System.out.println(queue.add("Dan") + " -- add");

      queue.add("Bob");

      System.out.println(queue.poll() + " -- poll");

      System.out.println(queue.peek() + " -- peek");

      System.out.println(queue.size() + " -- size");

      System.out.println(queue.contains("Dan") + " -- contains");

      System.out.println(queue.toArray()[1] + " -- toArray");

      System.out.println(queue.remove() + "-- remove");

      System.out.println(queue.offer("Dan") + " -- offer");

      System.out.println(queue.element() + "-- element");


   }
}
