package implementationQueue;

import java.util.Queue;
import java.util.Iterator;

public class Node<E>{
   public E value;
   public Node<E> next;

   public Node(E value){
      this.value = value;
   }

   public Node(){

   }

   public Node<E> getNext(){
      return this.next;
   }

   public void setNext(Node<E> next){
      this.next = next;
   }

   public E getValue(){
      return this.value;
   }

   public void setValue(E value){
      this.value = value;
   }
}
