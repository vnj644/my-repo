package implementationQueue;

import java.util.Iterator;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TestMyQueue{
   @Test void testGetRealSize(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      assertEquals(2, queue.getRealSize());
   }

   @Test void testIsEmpty(){
      MyQueue<String> queue = new MyQueue<String>();
      assertTrue(queue.isEmpty());
   }

   @Test void testHead(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      assertEquals(queue.head.getValue(),"A");
   }

   @Test void testTail(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      assertEquals(queue.tail.getValue(),"B");
   }

   @Test void testClear(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      Object[] mass = {};
      queue.clear();
      assertArrayEquals(mass, queue.toArray());
   }

   @Test void testContains(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      assertFalse(queue.contains("B"));
   }

   @Test void testContainsAll(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      MyQueue<String> queue1 = new MyQueue<String>();
      queue.add("C");
      queue.add("D");
      assertFalse(queue.contains(queue1));
   }

   @Test void testAddAll(){
      Object[] mass = {"A", "B", "C"};
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      MyQueue<String> queue1 = new MyQueue<String>();
      queue1.add("B");
      queue1.add("C");
      queue.addAll(queue1);
      assertArrayEquals(mass, queue.toArray());
   }

   @Test void testRemove1(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      Object[] mass = {"A"};
      queue.remove("B");
      assertArrayEquals(mass, queue.toArray());
   }

   @Test void testRemoveAll(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      queue.add("C");
      Object[] mass = {"A"};
      MyQueue<String> queue1 = new MyQueue<String>();
      queue1.add("B");
      queue1.add("C");
      queue.removeAll(queue1);
      assertArrayEquals(mass, queue.toArray());
   }

   @Test void testToArray1(){
      Object[] mass = {"A", "B"};
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      assertArrayEquals(mass, queue.toArray());
   }

   @Test void testToArray2(){
      Object[] mass = {"A", "B"};
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      Object[] testMass = new Object[0];
      assertArrayEquals(mass, queue.toArray(testMass));
   }

   @Test void testIterator(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      Iterator<String> iterator = queue.iterator();
      assertTrue(iterator.hasNext());
   }

   @Test void testSize(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      assertEquals(2, queue.size());
   }

   @Test void testElement(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      assertEquals("A", queue.element());
   }

   @Test void testPeek(){
      MyQueue<String> queue = new MyQueue<String>();
      assertNull(queue.peek());
      queue.add("A");
      assertEquals("A", queue.peek());
   }

   @Test void testAdd(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      Object[] mass = {"A"};
      assertArrayEquals(mass, queue.toArray());
   }

   @Test void testOffer(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.offer("A");
      Object[] mass = {"A"};
      assertArrayEquals(mass, queue.toArray());
   }

   @Test void testRemove2(){
      MyQueue<String> queue = new MyQueue<String>();
      queue.add("A");
      queue.add("B");
      queue.add("C");
      Object[] mass = {"B","C"};
      assertEquals("A", queue.remove());
      assertArrayEquals(mass, queue.toArray());
   }

   @Test void testPoll(){
      MyQueue<String> queue = new MyQueue<String>();
      assertNull(queue.poll());
      queue.add("A");
      queue.add("B");
      assertEquals("A", queue.poll());
      Object[] mass = {"B"};
      assertArrayEquals(mass, queue.toArray());

   }

}
