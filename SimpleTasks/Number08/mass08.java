public class mass08 {
    public static void main(String args[]){
        int[] mass = new int[] {1,2,3,4,5};
        for (int i = 0; i < mass.length - 1; i = i + 2) {
            int k = mass[i];
            mass[i] = mass[i+1];
            mass[i + 1] = k;
        }
        for (int g = 0; g < mass.length; g++ ){
          System.out.println(mass[g]);
        }
    }
}
