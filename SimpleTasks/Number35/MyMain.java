package Number35;

class MyMain{
   public static void main(String[] args) {
      MyStudentExcellent myStudentExcellent = new MyStudentExcellent("Даниил", "7.2");
      myStudentExcellent.setGroup("7.1");
      System.out.println(myStudentExcellent.getStudents());

      SchoolStaff schoolStaff = new SchoolStaff("Олег", "Директор");
      System.out.println(schoolStaff.getStaff());

      MyStudentExcellent[] myStudentExcellents = new MyStudentExcellent[5];
      myStudentExcellents[1] = new MyStudentExcellent("Даниил", "7.1");
      myStudentExcellents[2] = new MyStudentExcellent("Олег", "7.2");
      myStudentExcellents[3] = new MyStudentExcellent("Егор", "7.2");
      myStudentExcellents[4] = new MyStudentExcellent("Герман", "7.1");
      myStudentExcellents[5] = new MyStudentExcellent("Камиль", "7.1");
   }
}
