class Mass14{
   public static void main(String[] args) {
      int[] mass = new int[]{0, 1, 2, 6, 7, 8};
      int counter = 0;
      for (int i= 1;i < mass.length;i++ ) {
         if (mass[i] == mass[i - 1] + 1) {
            counter++;
         }
      }
      System.out.println(counter);
   }
}
