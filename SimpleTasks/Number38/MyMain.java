
class MyMain {
   public static void main(String[] args) {
      MyStudentExcellent myStudentExcellent = new MyStudentExcellent("Даниил", "7.2");
      myStudentExcellent.setGroup("7.1");
      System.out.println(myStudentExcellent.getStudents());

      MyStudentExcellent[] myStudentExcellents = new MyStudentExcellent[5];
      myStudentExcellents[0] = new MyStudentExcellent("Даниил", "7.1");
      myStudentExcellents[1] = new MyStudentExcellent("Олег", "7.2");
      myStudentExcellents[2] = new MyStudentExcellent("Егор", "7.2");
      myStudentExcellents[3] = new MyStudentExcellent("Герман", "7.1");
      myStudentExcellents[4] = new MyStudentExcellent("Камиль", "7.1");
   }
}
