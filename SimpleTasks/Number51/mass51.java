
public class mass51 {
    private String colour;
    public static int size = ((int) Math.random() * 40);
  
    public Cup1(String colour, int size) {
      this.colour = colour;
      Cup1.size = size;
    }
    public String getColour() {
      return colour;
    }
    public static int getSize() {
      return size;
    }

    public static void main(String[] args) {
        Cup1[] a = new Cup1[7];
        a[0] = new Cup1("Red", getSize());
        a[1] = new Cup1("Orange", getSize());
        a[2] = new Cup1("Yellow", getSize());
        a[3] = new Cup1("Green", getSize());
        a[4] = new Cup1("Blue", getSize());
        a[5] = new Cup1("DarkBlue", getSize());
        a[6] = new Cup1("Purple", getSize());
        for (int i = 0; i < 7; i++){
          System.out.println(a[i]);

        }
    }

}