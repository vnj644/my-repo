/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Factorial;

public class App {
    public static long factorial(int k){
      long end = 1;
      for(int i = 1; i <= k; i++){
    		end = end * i;
    	}
    	return end;
    }
    public static long factorialRec(long k){
      if (k == 1) {
        return k;
      }
      return k * factorialRec(k - 1);
    }
}
