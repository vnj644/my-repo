package Menu;

abstract class Action {
   String message;

  public Action(String message){
    this.message = message;
  }

  public String getMessage(){
    return this.message;
  }

  abstract public void act() throws Exception;
}
