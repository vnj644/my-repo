package Menu;

import java.util.*;

class ActionSubtraction extends Action {
   String message;

   public ActionSubtraction(String message){
      super(message);
   }

   public void act() throws Exception{
      Scanner sc = new Scanner(System.in);
      System.out.println("Введите первое число: ");
      int a = sc.nextInt();
      System.out.println("Введите второе число: ");
      int b = sc.nextInt();
      System.out.println("Разность чисел равна: " + subtraction(a,b));
   }

   public int subtraction(int a, int b){
      return a - b;
   }
}
