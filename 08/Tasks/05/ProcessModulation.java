import java.util.*;

class ProcessModulation{
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите количество повторений");
    int repeat = sc.nextInt();
    System.out.println("Введите размер области");
    int size = sc.nextInt();
    System.out.println("Введите сдвиг относительно начала координат");
    int shift = sc.nextInt();
    int system1 = repeat * 2;
    int system2 = 2;
    int[][] mass = new int[system1][system2];
    int max = shift + size;
    int min = shift - size;
    int counter = 0;
    for (int i = 0 ;i < repeat;i++ ) {
      for (int j = 0;j < system2;j++ ) {
        mass[i][j] = (int) (Math.random() * max) + min;
      }
      for (int g = system2;g < 4;g++ ) {
        mass[i + repeat][g - system2] = (int) (Math.random() * max) + min;
      }
    }
    for (int i = 0;i < repeat ;i++ ) {
      for (int j = i + 1;j < repeat ;j++ ) {
        if ((mass[j][0] > mass[i][0] && mass[j][0] < mass[i][1]) || (mass[j][1] > mass[i][0] && mass[j][1] < mass[i][1])){
          counter++;
        }
        else if ((mass[j][0] < mass[i][0] && mass[j][0] > mass[i][1]) || (mass[j][1] < mass[i][0] && mass[j][1] > mass[i][1])) {
           counter++;
        }
        else if (mass[j][0] < mass[i][0] && mass[j][1] > mass[i][1]) {
           counter++;
        }
      }
    }
    System.out.println(counter);
  }
}
