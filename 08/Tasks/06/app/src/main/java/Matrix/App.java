/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Matrix;

public class App {
  public static int[][][] matrixGeneration(int kol, int strings, int columns){
    int[][][] matrix = new int[kol][strings][columns];
    for (int i = 0; i < kol; i++) {
      for (int j = 0; j < strings; j++) {
        for (int q = 0; q < columns; q++) {
          int number = (int)(Math.random() * 90) + 10;
          matrix[i][j][q] = number;
        }
      }
    }
  return matrix;
  }

  public static int[][] matrixMultiply(int[][] matrix1, int[][] matrix2 ){
    int[][] answer = new int[matrix1.length][matrix2[0].length];
    for (int i = 0; i < matrix1[0].length; i++) {
      for (int j = 0; j < matrix2.length; j++) {
        for (int q = 0; q < matrix2[0].length; q++) {
          answer[i][j] = answer[i][j] + matrix1[i][q] * matrix2[q][j];
        }
      }
    }
    return answer;
  }
}
