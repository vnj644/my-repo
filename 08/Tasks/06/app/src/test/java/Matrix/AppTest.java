/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Matrix;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void matrix() {
      int[][] matrix1 = {{1, 2}, {3, 4}};
      int[][] matrix2 = {{6, 7}, {8, 9}};
      int[][] finalMatrix = {{22, 25}, {50, 57}};
      assertArrayEquals(finalMatrix, App.matrixMultiply(matrix1, matrix2));
    }
}
